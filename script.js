// String

// 1 Trim
// String.prototype.customTrim = function() {
//     let str = this.toString();
//     let bsh = 0;
//     let oxr = str.length - 1;
//     while (bsh < oxr && str[bsh] === " ") {
//         bsh++;
//     }
//     while (oxr > bsh && str[oxr] === " ") {
//         oxr--;
//     }
//     return str.slice(bsh, oxr + 1);
// };
// let str = "     Salom, Qalesila     ";
// console.log(str.customTrim()); 
// console.log(String.prototype);


// 2 UpperCas
// String.prototype.upperCase = function() {
//     let str = this.toString();
//     let natj = "";
//     for (let i = 0; i < str.length; i++) {
//         let charCode = str.charCodeAt(i);
//         if (charCode >= 97 && charCode <= 122) {
//             natj += String.fromCharCode(charCode - 32);
//         } else {
//             natj += str.charAt(i);
//         }
//     }
//     return natj;
// };
// let str2 = "Samariddin Nurmamatov";
// console.log(str2.upperCase());   
// console.log(String.prototype);


// 3 has
// String.prototype.has = function(substring) {
//     let str = this.toString();
//     return str.indexOf(substring) !== -1;
// };
// let str = "Salom, Bolla";
// console.log(str.has("Salom"));
// console.log(str.has("Bolla")); 
// console.log(String.prototype);

// 4 Cut
// String.prototype.cut = function(bsh_Index, oxrIndex) {
//     let str = this.toString();
//     if (oxrIndex === undefined) {
//         oxrIndex = str.length;
//     }
//     let res = "";
//     for (let i = bsh_Index; i < oxrIndex && i < str.length; i++) {
//         res += str[i];
//     }
//     return res;
// };
// let str = "Hello, World!";
// console.log(str.cut(0, 5)); 
// console.log(String.prototype);

// 5 Rpt
// String.prototype.rpt = function (count) {
//     let str = this.toString();
//     let Tk_str = '';
//     for (let i = 0; i < count; i++) {
//       Tk_str += str;
//     }
//     return Tk_str;
// };
// let str = "Hi!";
// console.log(str.rpt(3));
// console.log(String.prototype);
   

// ===========================================================
// Numberr

// 1 CustFixe 
// Number.prototype.customToFixed = function (digits) {
//     if (digits < 0 || digits > 20) {
//       throw new RangeError("toFixed() digits argument must be between 0 and 20");
//     }
//     let num = this.valueOf();
//     if (isNaN(num) || num === Infinity || num === -Infinity) {
//       return num.toString();
//     }
//     let s_kr = (num < 0 ? "-" : "");
//     let inPart = Math.floor(Math.abs(num)).toString();
//     let fractPart = "";
//     if (digits > 0) {
//       let multiplier = Math.pow(10, digits);
//       fractPart = "." + (Math.round((Math.abs(num) - inPart) * multiplier)).toString().padStart(digits, "0");
//     }
//     return s_kr + inPart + fractPart;
// }
// let num = 3.14159;
// console.log(num.customToFixed(2)); 
// console.log(Number.prototype);
  
// 2 Round
// Number.prototype.customRound = function() {
//     let num = this.valueOf();
//     let absNum = Math.abs(num);
//     let rouNum = Math.floor(absNum + 0.5);
//     return (num < 0) ? -rouNum : rouNum;
// }
// let nu1 = 4.5874;
// console.log(nu1.customRound()); 
// let nu2 = 10.302;
// console.log(nu2.customRound()); 
// console.log(Number.prototype);
  
// 3 isSqur
// Number.prototype.isSquare = function() {
//     let nu = this.valueOf();
//     let sqrt_Nu = Math.sqrt(nu);
//     return sqrt_Nu % 1 === 0;
// }
// let nu1 = 15;
// console.log(num1.isSquare()); // false
// let nu2 = 16;
// console.log(nu2.isSquare()); // true
// console.log(Number.prototype);

// 4 Count
// Number.prototype.count = function() {
//     let num = Math.abs(this.valueOf());
//     return num.toString().length;
// }
// let nu1 = -676;
// console.log(nu1.count()); 
// let nu2 = 6780;
// console.log(nu2.count());  
// console.log(Number.prototype);
  
// 5 Sum
// Number.prototype.sum = function() {
//     let num = Math.abs(this.valueOf());
//     let sum = 0;
//     while (num > 0) {
//       sum += num % 10;
//       num = Math.floor(num / 10);
//     }
//     return sum;
// }
// let nu1 = -456;
// console.log(nu1.sum()); 
// let nu2 = 7890;
// console.log(nu2.sum()); 
// console.log(Number.prototype);




// Array
// 1 CustomMap
// Array.prototype.customMap = function(callback) {
//     let result = [];
//     for (let i = 0; i < this.length; i++) {
//       result.push(callback(this[i], i, this));
//     }
//     return result;
// }
// let arr1 = [1, 2, 3];
// let result1 = arr1.customMap((item) => item * 2);
// console.log(result1);
// console.log(Array.prototype);

// 2 Ever
// Array.prototype.customEvery = function(callback) {
//     for (let i = 0; i < this.length; i++) {
//       if (!callback(this[i], i, this)) {
//         return false;
//       }
//     }
//     return true;
// }
// const arr = [1, 2, 3, 4, 5];
// const isEven = (num) => num % 2 === 0;
// console.log(arr.customEvery(isEven)); // false, 

// const evenArr = [2, 4, 6, 8];
// console.log(evenArr.customEvery(isEven)); // true,
// console.log(Array.prototype);

// 3 Redus
// Array.prototype.customReduce = function (callback, initialValue) {
//   let accumulator = initialValue === undefined ? undefined : initialValue;
//   for (let i = 0; i < this.length; i++) {
//     if (accumulator !== undefined) {
//       accumulator = callback.call(undefined, accumulator, this[i], i, this);
//     } else {
//       accumulator = this[i];
//     }
//   }
//   return accumulator;
// };
// let arr = [1, 2, 3, 4, 5];
// console.log(arr.customReduce((acc, cur) => acc + cur)); 
// console.log(Array.prototype);
  
// 4 FindIndex
// Array.prototype.customFindIndex = function (callback) {
//     for (let i = 0; i < this.length; i++) {
//       if (callback(this[i], i, this)) {
//         return i;
//       }
//     }
//     return -1;
// };
// const arr = [2, 4, 6, 8, 10];
// const index = arr.customFindIndex((element) => element === 8);
// console.log(index);
// console.log(Array.prototype);
  
// 5 Splic
// Array.prototype.customSplice = function(start, deleteCount, ...items) {
//   const deletedItems = [];
//   const len = this.length;

//   let actualStart;
//   if (start === undefined) {
//     actualStart = 0;
//   } else if (start < 0) {
//     actualStart = len + start;
//     if (actualStart < 0) {
//       actualStart = 0;
//     }
//   } else if (start > len) {
//     actualStart = len;
//   } else {
//     actualStart = start;
//   }

//   let actualDeleteCount;
//   if (deleteCount === undefined || deleteCount > len - actualStart) {
//     actualDeleteCount = len - actualStart;
//   } else if (deleteCount < 0) {
//     actualDeleteCount = 0;
//   } else {
//     actualDeleteCount = deleteCount;
//   }

//   for (let i = 0; i < actualDeleteCount; i++) {
//     const currentIndex = actualStart + i;
//     if (currentIndex in this) {
//       const currentElement = this[currentIndex];
//       deletedItems.push(currentElement);
//     }
//   }

//   const itemCount = items.length;
//   const length = this.length - actualDeleteCount + itemCount;

//   for (let i = len - 1; i >= actualStart + actualDeleteCount; i--) {
//     const fromIndex = i;
//     const toIndex = i + itemCount - actualDeleteCount;
//     if (fromIndex in this) {
//       this[toIndex] = this[fromIndex];
//     } else {
//       delete this[toIndex];
//     }
//   }

//   for (let i = 0; i < itemCount; i++) {
//     this[actualStart + i] = items[i];
//   }

//   this.length = length;

//   return deletedItems;
// };
// const arr = [1, 2, 3, 4, 5];
// console.log(arr.customSplice(1, 2, "a", "b")); 
// console.log(arr); 
// console.log(Array.prototype);




//============================================================================
// 1 Animal
// function Animal(name, speed, limitAge) {
//     this.name = name;
//     this.speed = speed;
//     this.limitAge = limitAge;
// }
// Animal.prototype.info = function() {
//     return `Name: ${this.name}, Speed: ${this.speed}, LimitAge: ${this.limitAge}`;
// }  
// const Tiger = new Animal("Tiger", 60, 16);
// console.log(Tiger.info());

// 2 Student
// function Student(name, course, years, university) {
//     this.name = name;
//     this.course = course;
//     this.years = years;
//     this.university = university;
// }
// Student.prototype.leftYears = function() {
//     return this.years < 4 ? 4 - this.years : 0;
// }  
// const samariddin = new Student("Samariddin", "Computer", 3, "Tatu University");
// console.log(samariddin.leftYears());


// 3 Person
// function Person(name, age, currentYear) {
//     this.name = name;
//     this.age = age;
//     this.currentYear = currentYear;
// }
// Person.prototype.getBirthYear = function() {
//     return this.currentYear - this.age;
// }  
// const samariddin = new Person("Samariddin", 16, 2023);
// console.log(samariddin.getBirthYear());


// 4 Emplyo
// function Employee(name, salary, workName) {
//     this.name = name;
//     this.salary = salary;
//     this.workName = workName;
// }
// Employee.prototype.increaseSalary = function(percent) {
//     const increaseAmount = this.salary * percent / 100;
//     return this.salary + increaseAmount;
// }
// const employee = new Employee("Ali", 5000, "Front-End Dev");
// console.log(employee.increaseSalary(10)); 


// 5 Rectangl
// function Rectangle(width, height) {
//     this.width = width;
//     this.height = height;
// }
// Rectangle.prototype.getArea = function() {
//     return this.width * this.height;
// } 
// Rectangle.prototype.getPerimeter = function() {
//     return 2 * (this.width + this.height);
// }
// const rectangle = new Rectangle(10, 20);
// console.log(rectangle.getArea()); 
// console.log(rectangle.getPerimeter()); 
